﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    public class KetQuaNgay
    {
        public DateTime Ngay { get; set; }
        public int Ketqua { get; set; }
    }

    public enum eBoSo
    {
        Bo1,
        Bo2,
        Bo3
    }

    public class BoSo
    {
        public List<BoSoDetail> config = new List<BoSoDetail>();
        public BoSo()
        {
            this.config.Add(new BoSoDetail());
        }
        public long TongTien() => config.Sum(a => a.TongTienHienTaiNhom);
    }

    public class CONST
    {
        public  static int TileTrung = 80;
    }

    public class BoSoDetail
    {
        [JsonProperty("MuSo")]
        public int MuSoConfig { get; set; }

        [JsonProperty("BoSo")]
        public List<int> BosoConfig { get; set; }

        [JsonProperty("TenBoSo")]
        public eBoSo BoSoName { get; set; }

        [JsonProperty("TienBatDau")]
        public int StartMoneyconfig { get; set; }

        [JsonProperty("NgayNghi")]
        public int NgayNghi { get; set; } = 0;


        [JsonProperty("TienCuocBatDau")]
        public int TienCuocBatDau { get; set; } = 1000;

        public long TongTienHienTaiNhom { get; set; } = 0;
        public int TienCuocHienTai { get; set; } = 1000;
       
        public List<DateTime> NgayTrungsHienTais { get; set; } = new List<DateTime>();

        
        public void Process (KetQuaNgay ketQuaNgay)
        {
           
            if (this.BosoConfig.Contains(ketQuaNgay.Ketqua))
            {
                this.TongTienHienTaiNhom += this.TienCuocHienTai * CONST.TileTrung;
                this.TienCuocHienTai = this.TienCuocBatDau;
                this.NgayTrungsHienTais.Add(ketQuaNgay.Ngay);
            }
            else
            {
                this.TienCuocHienTai = this.TienCuocHienTai * this.MuSoConfig;
            }

            if (this.CheckCoChoi(ketQuaNgay.Ngay.AddDays(1)))
            {
                this.TongTienHienTaiNhom = this.TongTienHienTaiNhom - this.TienCuocHienTai * this.BosoConfig.Count;
            }

        }

        private bool CheckCoChoi(DateTime ngay)
        {
            if (NgayTrungsHienTais.Count >= 2)
            {
                var item1 = this.NgayTrungsHienTais[this.NgayTrungsHienTais.Count - 2];
                var item2 = this.NgayTrungsHienTais[this.NgayTrungsHienTais.Count - 1];
                if (item1 == item2)
                {
                    if ((ngay - item1).TotalDays <= this.NgayNghi)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }

}
