﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sode
{
    public class Worker3
    {
        ILog log = LogManager.GetLogger(typeof(Worker3));
        double tongTienHienTai = 0;
        int TiLeLai = 90;
        int soTienBatDau = 1000;

        public void Run(DateTime startDate, double startMoney)
        {
            
            tongTienHienTai = startMoney;

            var ketqua = KetQuaService.ReadKetQua().Where(a => a.Ngay >= startDate).ToList().OrderBy(a => a.Ngay).ToList();
            List<BoSo3> CauHinhDanhs = KetQuaService.ReadCauHinhBoSo3();
            for (int i = 0; i < ketqua.Count; i++)
            {
                this.log.Info($"\n\n Ngay: {ketqua[i].Ngay.ToString("yyyy-MM-dd")} TongTien: {String.Format("{0:n0}", this.tongTienHienTai)}");
                foreach (var cauHinhDanh in CauHinhDanhs)
                {
                    bool trung = cauHinhDanh.DuLieuCauHinhBoSo3s.Data.Contains(ketqua[i].Ketqua);
                    
                    if (trung)
                    {
                        tongTienHienTai += cauHinhDanh.SoTienDanh * TiLeLai;
                        cauHinhDanh.SoTienDanh = soTienBatDau;
                    }
                    else
                    {
                        cauHinhDanh.SoTienDanh = cauHinhDanh.SoTienDanh * cauHinhDanh.DuLieuCauHinhBoSo3s.LuyThuaNang;
                    }
                    if (cauHinhDanh.SoTienDanh == 0) cauHinhDanh.SoTienDanh = soTienBatDau;
                    var tongTienDanh = cauHinhDanh.DuLieuCauHinhBoSo3s.Data.Count * cauHinhDanh.SoTienDanh;
                    tongTienHienTai = tongTienHienTai - tongTienDanh;
                    this.log.Info($"Boso: {cauHinhDanh.DuLieuCauHinhBoSo3s.Name} Trung: {trung} Danh: {cauHinhDanh.SoTienDanh}*{cauHinhDanh.DuLieuCauHinhBoSo3s.Data.Count}={String.Format("{0:n0}", tongTienDanh)}");
                }
                string mssLog = $"NGay: {ketqua[i].Ngay.ToString("yyyy-MM-dd")} SoTrung: {ketqua[i].Ketqua} TongTien: {this.tongTienHienTai}";
                this.log.Info(mssLog);
                if (tongTienHienTai < 0)
                {
                    this.log.Info("HetTien");
                    return;
                }
            }

        }
    }
}
