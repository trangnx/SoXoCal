﻿using log4net;
using Sode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApp12
{
    public  class WorkerRun
    {

        DateTime startDate = DateTime.Now;
        BoSo bosoConfig = new BoSo();
        ILog log = LogManager.GetLogger(typeof(WorkerRun));

        public void Run(DateTime ngayBatDau, long soTienMax=10000000000)
        {
            List<KetQuaNgay> ketqua = KetQuaService.ReadKetQua() ;
            this.startDate = ngayBatDau;
            log.Debug("Loading ket qua ... ");
            var boChay = ketqua.Where(a => a.Ngay >= startDate).ToList().OrderBy(a => a.Ngay);
            log.Debug("Loaded ket qua: " + boChay.Count());
            CONST.NgayDauTien = boChay.First().Ngay;
            foreach(var b in this.bosoConfig.config)
            {
                b.XuLy(ketqua);
            }
            bosoConfig.RunWithMoney(1000000000, boChay.ToList());
        }

        

       

    }
}
