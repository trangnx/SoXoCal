﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sode
{
    public class DataCuoiCungVe
    {
        public DateTime NgayCuoiCung { get; set; }
    }

    public class Worker4
    {
        double tongTien = 0;
        int ngayBatDauDanh = 20;
        int soTienThemMoiNgay = 1000;
        int tiLeThuong = 90;
        ILog log = LogManager.GetLogger(typeof(Worker4));

        List<Tuple<int, int>> duLieuDanhHomNay = new List<Tuple<int, int>>();
        Dictionary<int, DateTime> dicLanCuoiCungVe = new Dictionary<int, DateTime>();

        public void Run(DateTime startDate, double tongTien)
        {
            //Cập nhật lần về cuối cùng là Min
            InitData(dicLanCuoiCungVe, startDate);


            this.tongTien = tongTien;
            this.log.Info("Start load ketqua tu file");
            var ketqua = KetQuaService.ReadKetQua().Where(a => a.Ngay >= startDate).ToList().OrderBy(a => a.Ngay).ToList();


            for (int i = 0; i < ketqua.Count; i++)
            {
                

                //Cập nhật thông tin ngày cuối cùng về
                foreach(var itemInDic in dicLanCuoiCungVe)
                {
                    //Nếu kết quả có chứa ngày cuối cùng về thì cập nhật lại thông tin trong dic
                    if (ketqua[i].Ketqua.ToString().PadLeft(2,'0').Contains(itemInDic.Key.ToString()))
                    {
                        dicLanCuoiCungVe[itemInDic.Key] = ketqua[i].Ngay;
                    }
                }

                //Tính toán lại tiền nếu trúng nếu trúng
                if (duLieuDanhHomNay.Any(a=>a.Item1== ketqua[i].Ketqua))
                {
                    foreach (var t in duLieuDanhHomNay.Where(a => a.Item1 == ketqua[i].Ketqua))
                    {
                        int lai = t.Item2 * tiLeThuong;
                        tongTien = tongTien + lai;
                        this.log.Info($"Ngay: {ketqua[i].Ngay.ToString("yyyy-MM-dd")}");
                        this.log.Info($"Trung: {ketqua[i].Ketqua} => Lai: {t.Item2}*{tiLeThuong}={lai} => TongTien: {tongTien}");
                    }
                }

                //Cập nhật dữ liệu đánh
                duLieuDanhHomNay.Clear();
                foreach (var lanCuoiCungVe in dicLanCuoiCungVe)
                {
                    //Chỉ đánh khi lần cuối cùng về với ngày hiện tại lệch nhau quá 20 ngày
                    if (lanCuoiCungVe.Value != DateTime.MinValue && (ketqua[i].Ngay - lanCuoiCungVe.Value).TotalDays > 20)
                    {
                        int tienDanh = 0;
                        int soNgay = (int)(ketqua[i].Ngay - lanCuoiCungVe.Value).TotalDays - 20;
                        int tienCuoc = (2 ^ soNgay) * (soTienThemMoiNgay);
                        foreach(var itemSo in DanhSachSo(lanCuoiCungVe.Key))
                        {
                            duLieuDanhHomNay.Add(new Tuple<int, int>(itemSo, tienCuoc));
                            tienDanh += tienCuoc;
                            this.tongTien = tongTien - tienCuoc;
                        }
                        this.log.Info($"Danh so: {lanCuoiCungVe.Key} voi so tien: {tienCuoc} TongTien: {this.tongTien} TienCuoc: {tienDanh}");
                    }
                }
            }

        }

        private IEnumerable<int> DanhSachSo(int key)
        {
            List<int> lst = new List<int>();
            for (int i = 0; i < 9; i++)
            {
                lst.Add(Convert.ToInt32($"{i}{key}"));
                lst.Add(Convert.ToInt32($"{key}{i}"));

            }
            return lst;
        }

        private void InitData(Dictionary<int, DateTime> dicLanCuoiCungVe, DateTime startDate)
        {
            for (int i = 0; i < 9; i++)
            {
                this.dicLanCuoiCungVe.Add(i, DateTime.MinValue);
            }
        }
    }
}
