﻿using ConsoleApp12;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sode
{

    public interface IThuatToan
    {

    }

    public class WorkerRunV2
    {
        private IThuatToan _thuatToan;
        private List<KetQuaNgay> _ketQuaNgay;
        private long _tongTienCongLai = 0;

        public WorkerRunV2 (IThuatToan thuatToan, List<KetQuaNgay> ketQuaNgay)
        {
            this._thuatToan = thuatToan;
            this._ketQuaNgay = ketQuaNgay;
        }

        public void Start (DateTime startDate, int totalStart)
        {
            this._tongTienCongLai = totalStart;

        }

    }
}
