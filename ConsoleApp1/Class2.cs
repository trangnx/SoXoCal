﻿using ConsoleApp12;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sode
{
    public class KetQuaService
    {
        public static List<KetQuaNgay> ReadKetQua()
        {
            var lst = File.ReadAllLines("KetQuaCrawl.txt").Select(a =>
            {
                var datas = a.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                return new KetQuaNgay()
                {
                    Ketqua = Convert.ToInt32(datas[1]),
                    Ngay = Convert.ToDateTime(datas[0])
                };
            }).ToList();
            return lst.OrderBy(a => a.Ngay).ToList();
        }

        internal static List<BoSo3> ReadCauHinhBoSo3()
        {
            List<DuLieuCauHinhBoSo3> data = JsonConvert.DeserializeObject<List<DuLieuCauHinhBoSo3>>(File.ReadAllText("BoSo3.json"));
            foreach(var item in data)
            {
                if (item.Data.Count != item.Data.Distinct<int>().Count())
                {
                    foreach(var x in item.Data.Distinct<int>())
                    {
                        if (item.Data.Where(a => a == x).Count() > 1)
                        {
                            Console.WriteLine(x);
                            throw new Exception();
                        }
                    }
                }
            }
            return data.Select(a => new BoSo3()
            {
                DuLieuCauHinhBoSo3s = a,
                SoTienDanh = 0
            }).ToList();
        }
    }
}
