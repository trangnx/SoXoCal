﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sode
{
    class Worker1
    {
        ILog log = LogManager.GetLogger(typeof(Worker1));
        public class SoThongKe
        {
            public int SoNgayMaxKhongRa { get; set; } = 0;
            public DateTime LanCuoiCung { get; set; } = new DateTime(2015,1,1);
        }

        public void Run (DateTime dtFrom)
        {

            var ketqua = KetQuaService.ReadKetQua().Where(a => a.Ngay >= dtFrom).ToList().OrderBy(a => a.Ngay).ToList();
            var ngayBatDau = ketqua[0].Ngay;
            log.Info("Khoi tao dic");
           
            // Số ngày max hiện tại
            Dictionary<int, SoThongKe> dicResult = new Dictionary<int, SoThongKe>();

            for (int i = 0; i < 10; i++)
            {
                dicResult.Add(i, new SoThongKe()
                {
                    LanCuoiCung = ngayBatDau,
                    SoNgayMaxKhongRa = 0
                });
            }

            for (int i = 0; i < ketqua.Count; i++)
            {
                var ketQuaDay = ketqua[i];
                //log.Info($"Ngay : {ketQuaDay.Ngay.ToString("yyyy-MM-dd")}=> {ketQuaDay.Ketqua}");

                foreach(var item in dicResult)
                {
                    if (ketQuaDay.Ketqua.ToString().PadLeft(2,'0').Contains(item.Key.ToString()))
                    {
                        dicResult[item.Key].LanCuoiCung = ketQuaDay.Ngay;
                    }
                    else
                    {
                        int soNgayKhongRa = (int)(ketQuaDay.Ngay - dicResult[item.Key].LanCuoiCung).TotalDays;
                        if (soNgayKhongRa > dicResult[item.Key].SoNgayMaxKhongRa)
                        {
                            dicResult[item.Key].SoNgayMaxKhongRa = soNgayKhongRa;
                            if (soNgayKhongRa>38)
                            {
                                this.log.Info($"{item.Key} {ketQuaDay.Ngay.ToString("yyyyy-MM-dd")} fail at {item.Value.SoNgayMaxKhongRa}");
                            }
                        }
                    }
                }
            }

            foreach(var item in dicResult)
            {
                this.log.Info($"{item.Key} => {item.Value.SoNgayMaxKhongRa}");
            }
        }
    }
}
