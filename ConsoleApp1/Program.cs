﻿using Sode;
using System;
using System.IO;
using System.Reflection;
using System.Xml;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));
            var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);

            //var clr = new CrlData();
            //clr.Process(new DateTime(2021, 12, 1), new DateTime(2022, 02, 18));

            //CrlData crl = new CrlData();
            //crl.ProcessForDate(new DateTime(2016, 02, 03));

            //WorkerRun w = new WorkerRun();
            //w.Run(new DateTime(2015, 1, 1), soTienMax: 1000000000);


            //var worker2 = new Worker1();
            //worker2.Run(new DateTime(2021,1,1));

            //var worker3 = new Worker3();
            //worker3.Run(new DateTime(2015, 1, 1), 10000000000);


            var worker4 = new Worker4();
            worker4.Run(new DateTime(2015, 1, 1), 200000000);


            Console.ReadLine();
        }
    }
}
