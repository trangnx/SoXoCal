﻿using HtmlAgilityPack;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sode
{
    public class CrlData
    {
        List<string> ketquas = new List<string>();
        public void ProcessForDate(DateTime date)
        {
            int retry = 0;
            try
            {
                string link = $"https://www.minhngoc.net.vn/ket-qua-xo-so/mien-bac/{date.ToString("dd-MM-yyyy")}.html";
                RestClient restClient = new RestClient();
                var res = restClient.Execute(new RestRequest(link));
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var html = res.Content;
                    var doc = new HtmlDocument();
                    doc.LoadHtml(html);
                    var nodeBangs = doc.DocumentNode.SelectNodes(@"//div[@class='content']//table");
                    if (nodeBangs != null && nodeBangs.Count > 0)
                    {

                        var nodeBang = nodeBangs.First();
                        if (nodeBang.InnerText.Contains(date.ToString("dd/MM/yyyy").Replace('-','/')))
                        {
                            if (nodeBang.InnerText.ToLower().Contains("ký hiệu") || nodeBang.InnerText.Contains("XSMB"))
                            {
                                var nodes = nodeBang.SelectNodes(".//td[@class='giaidb'][1]");
                                if (nodes != null)
                                {
                                    string texxt = nodes.First().InnerText.Trim();
                                    string kq = $"{date.ToString("yyyy-MM-dd")} {texxt.Substring(texxt.Length - 2, 2)}";
                                    if (Regex.IsMatch(texxt, @"\d+"))
                                    {
                                        this.ketquas.Add(kq);
                                        Console.WriteLine($"{date.ToString("yyyy-MM-dd")}->{kq}");
                                    }
                                }
                            }
                        }
                    }
                }
           
                else
                {
                    Console.WriteLine($"Error html: {res.StatusCode.ToString()} {link} ");
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        

        public void Process(DateTime start, DateTime end)
        {
            var currentDate = start;
            List<DateTime> lst = new List<DateTime>();
            while (currentDate < end)
            {
                lst.Add(currentDate);
                currentDate = currentDate.AddDays(1);
            }
            Parallel.ForEach(lst, new ParallelOptions()
            {
                MaxDegreeOfParallelism = 2
            }, (a) => ProcessForDate(a));

            this.ketquas.Sort();
            File.WriteAllLines("KetQua.txt", ketquas);
            Console.WriteLine("Write to ketqua.txt");

        }


    }
}
