﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sode
{
    public class BoSo3
    {
        public DuLieuCauHinhBoSo3 DuLieuCauHinhBoSo3s = new DuLieuCauHinhBoSo3();

        public int SoTienDanh { get; set; }

    }

    public class DuLieuCauHinhBoSo3
    {
        public string Name { get; set; }
        public List<int> Data { get; set; }
        public int LuyThuaNang { get; set; } = 2;
    }
}
