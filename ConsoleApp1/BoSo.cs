﻿using log4net;
using Newtonsoft.Json;
using Sode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    public class KetQuaNgay
    {
        public DateTime Ngay { get; set; }
        public int Ketqua { get; set; }

        

       
    }

    public class DuLieuChoiNgay
    {
        public bool? IsChoi = null;
        public long TienCuoc { get; set; }
        public bool IsTrung { get; set; }
        public DateTime Ngay { get; set; }
        public long TongHienTai { get; set; }
        public int KetQua { get;  set; }
    }



    public class BoSo
    {
        ILog log = LogManager.GetLogger(typeof(BoSo));

        public List<BoSoDetail> config = new List<BoSoDetail>();
        public BoSo()
        {
            this.config = JsonConvert.DeserializeObject<List<BoSoDetail>>(File.ReadAllText("BoDe.json"));
            foreach(var b in this.config)
            {
                this.log.Info(b.BoSoName + " : BosoCOUNT: " + b.BosoConfig.Count);
            }

            for (int i = 0; i < 99; i++)
            {
                var count = config.Count(a => a.BosoConfig.Contains(i));
                if (count!=1)
                {
                    log.Error("Bo so nhap vao khong du: " + i + $" count: " + count);
                }

            }
        }

        internal void RunWithMoney(long inputtong, List<KetQuaNgay> boChay)
        {

            this.log.Info("-------------------------");
            for (int i = 0; i < boChay.Count(); i++)
            {
                long tongTien = inputtong;
                var strLog = new StringBuilder();
                strLog.Append($"\n\n {boChay[i].Ngay.ToString("yyyy-MM-dd")}  {boChay[i].Ketqua}");
                for (var j = 0; j < config.Count; j++)
                {
                    tongTien = tongTien + this.config[j].duLieuChoiNgays[i].TongHienTai;
                    var isChoi = (config[j].duLieuChoiNgays[i].IsChoi.Value) ? "1" : "0";
                    strLog.Append($"\n {config[j].BoSoName.ToString()} | {config[j].duLieuChoiNgays[i].KetQua} | Choi: {isChoi} | Trung: {(config[j].duLieuChoiNgays[i].IsTrung ? "1":"0")} | Cuoc: {config[j].duLieuChoiNgays[i].TienCuoc} | TT: {config[j].duLieuChoiNgays[i].TongHienTai} =>  {tongTien}");
                }
                strLog.Append($"\n => {tongTien}");
                if (tongTien <= 0)
                {
                    log.Info(strLog.ToString());
                    log.Info("Het tien");
                    Console.ReadLine();
                }
                  
                log.Info(strLog.ToString());
            }


        }
    }

    public class CONST
    {
        public  static int TileTrung = 90;
        public static DateTime NgayDauTien = DateTime.MinValue;

    }

    public class BoSoDetail
    {
        ILog log = LogManager.GetLogger(typeof(BoSoDetail));
        public List<DuLieuChoiNgay> duLieuChoiNgays = new List<DuLieuChoiNgay>();

        [JsonProperty("MuSo")]
        public int MuSoConfig { get; set; }


        internal void XuLy(List<KetQuaNgay> ketqua)
        {

            this.log.Debug("Tinh toan du lieu choi ngay");
            for (int i = 0; i < ketqua.Count; i++)
            {
                if (ketqua[i].Ngay == new DateTime(2020, 12, 04))
                {
                    Console.WriteLine("bug");
                }
                var data = new DuLieuChoiNgay()
                {
                    Ngay = ketqua[i].Ngay,
                    IsTrung = this.BosoConfig.Contains(ketqua[i].Ketqua),
                    KetQua= ketqua[i].Ketqua
                };
                this.duLieuChoiNgays.Add(data);
                
            }

            duLieuChoiNgays[0].IsChoi = false;
            duLieuChoiNgays[1].IsChoi = true;


            this.log.Debug("Tinh toan ngay nghi");

            this.duLieuChoiNgays[0].IsChoi = false;
            for (int i = 0; i < this.duLieuChoiNgays.Count; i++)
            {
                if (this.duLieuChoiNgays[i].IsTrung)
                {
                    for (int j = 0; j < this.SoNgayNghi; j++)
                    {
                        if (i + j + 1 < duLieuChoiNgays.Count)
                        {
                            this.duLieuChoiNgays[i + j + 1].IsChoi = false;
                        }
                    }
                }
                if (!this.duLieuChoiNgays[i].IsChoi.HasValue) this.duLieuChoiNgays[i].IsChoi = true;
            }

            this.log.Debug("Tinh toan tien choi");
            duLieuChoiNgays[0].TienCuoc = this.TienCuocBatDauConfig;
            for (int i = 1; i < this.duLieuChoiNgays.Count; i++)
            {
                if (duLieuChoiNgays[i - 1].IsChoi.Value)
                {
                    if (duLieuChoiNgays[i - 1].IsTrung)
                    {
                        this.duLieuChoiNgays[i].TienCuoc = this.TienCuocBatDauConfig;
                    }
                    else
                    {
                        this.duLieuChoiNgays[i].TienCuoc = this.duLieuChoiNgays[i - 1].TienCuoc * this.MuSoConfig;
                    }
                }
                else
                {
                    this.duLieuChoiNgays[i].TienCuoc = this.duLieuChoiNgays[i - 1].TienCuoc;
                }
            }

            this.log.Debug("Tinh toan tien hien tai");
            for (int i = 0; i < this.duLieuChoiNgays.Count; i++)
            {
                if (i == 0)
                {
                    this.duLieuChoiNgays[i].TongHienTai = 0;
                }
                else
                {
                    long tienTruoc = (i == 0) ? 0 : (this.duLieuChoiNgays[i - 1].TongHienTai);
                    long tienBienDong = 0;
                    if (this.duLieuChoiNgays[i].IsChoi.Value)
                    {
                        tienBienDong = ((this.duLieuChoiNgays[i].IsTrung) ? (this.duLieuChoiNgays[i].TienCuoc * CONST.TileTrung) : 0)
                            - ((this.duLieuChoiNgays[i].TienCuoc) * this.BosoConfig.Count);
                    }
                    this.duLieuChoiNgays[i].TongHienTai = duLieuChoiNgays[i - 1].TongHienTai + tienBienDong;
                }
            }

            this.log.Debug("===>");
            for (int i = 0; i < this.duLieuChoiNgays.Count; i++)
            {
                string tr = duLieuChoiNgays[i].IsTrung ? "Trung" : "Truot";
                this.log.Debug($" Date: {duLieuChoiNgays[i].Ngay.ToString("dd/MM/yy")} Choi: {duLieuChoiNgays[i].IsChoi} {tr} Cuoc: {duLieuChoiNgays[i].TienCuoc} => {duLieuChoiNgays[i].TongHienTai}");
            }
        }

        private bool CheckNgayChoi(DateTime ngay)
        {
            throw new NotImplementedException();
        }

        [JsonProperty("BoSo")]
        public List<int> BosoConfig { get; set; }

        [JsonProperty("TenBoSo")]
        public eBoSo BoSoName { get; set; }


     
        [JsonProperty("NghiKep")]
        public bool NghiKep { get; set; } = false;

        [JsonProperty("TienCuocBatDau")]
        public int TienCuocBatDauConfig { get; set; } = 1000;






        public long TongTienHienTaiNhom { get; set; } = 0;
        public int TienCuocHienTai { get; set; } = 1000;

        public List<DateTime> NgayTrung { get; set; } = new List<DateTime>();

        [JsonProperty("SoNgayNghi")]
        public int SoNgayNghi { get; private set; } = 0;
    }

}
